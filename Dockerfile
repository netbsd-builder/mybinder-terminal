FROM python:3.9-alpine
# install docker dev packages
RUN apk -f update
RUN apk add alpine-base sudo alpine-sdk libffi-dev linux-headers
# build.sh depends
RUN apk add zlib zlib-dev
RUN apk add docker

# install the notebook package
RUN pip install --no-cache --upgrade pip && \
    pip install --no-cache notebook jupyterlab


# create user with a home directory
ARG NB_USER
ARG NB_UID
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}
WORKDIR ${HOME}
USER ${USER}

RUN git clone -b anvil https://gitlab.com/netbsd-builder/src.git
#RUN cd src; git pull; ./build.sh -m amd64 -uU -j5 tools
